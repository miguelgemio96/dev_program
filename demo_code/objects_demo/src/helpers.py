from datetime import datetime


FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'
NONE_ERROR_MESSAGE = 'start_dt or end_dt cannot be None'
INSTANCE_ERROR_MESSAGE = 'start_dt and end_dt must be DateTimeWrapper instances'


class DateTimeWrapper:
    """ This class wraps native a native `datetime` """
    def __init__(self, datetime_str, format=FORMAT_SHORT_YEAR):
        """
        This constructs a DateTimeWrapper object
        :param datetime_str: A str containing datetime value
        :param format: A str that holds the format of the datetime_str
        """
        self.__native_datetime = datetime.strptime(datetime_str, format)

    @property
    def datetime(self):
        return self.__native_datetime


def check_valid_inputs_DateTimeWrapper(start_dtw, end_dtw):
    """
    This function validate if the inputs are valid to be proccessed.
    :param start_dt: A DateTimeWrapper object.
    :param end_dt: A DateTimeWrapper object.
    :return: None.
    :raise: raise an Exceptcion of ValueError in case of an invalid input.
    """
    if not start_dtw or not end_dtw:
        raise ValueError(NONE_ERROR_MESSAGE)

    if not isinstance(start_dtw, DateTimeWrapper) or not isinstance(end_dtw, DateTimeWrapper):
        raise ValueError(INSTANCE_ERROR_MESSAGE)


def calculate_duration_hh_mm_ss(start_dtw, end_dtw):
    """
    This function calculates duration between to datetime values
    :param start_dt: A DateTimeWrapper object
    :param end_dt: A DateTimeWrapper object
    :return: A tuple of integers that holds hours, minutes, seconds values
    """
    check_valid_inputs_DateTimeWrapper(start_dtw, end_dtw)
    diff = end_dtw.datetime - start_dtw.datetime
    seconds = abs(diff.total_seconds())
    mm, ss = divmod(seconds, 60)
    hh, mm = divmod(mm, 60)
    return int(hh), int(mm), int(ss)


def get_total_seconds(h, m, s):
    """
    This a public function calculate total seconds calculated from the values
    of hours, minutes and seconds.
    :param h: A Int that represent the hours.
    :param m: A int that represent the minutes.
    :param a: A int that represent the seconds.
    :return: int with the total seconds..
    """
    total_seconds = 0
    if h is None or m is None or s is None:
        return total_seconds
    total_seconds = (h * 3600) + (60 * m) + s
    return total_seconds
