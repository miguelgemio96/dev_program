from src.helpers import calculate_duration_hh_mm_ss, get_total_seconds


class Duration:
    """
    The Duration class represents a time value like 1h 32m 38s
    """

    def __init__(self, hours, minutes, seconds):
        """
        This constructs a Duration object
        :param hours: A int containing the hours value
        :param minutes: A int containing the minutes value
        :param seconds: A int containing the seconds value
        """
        self.__hours = hours
        self.__minutes = minutes
        self.__seconds = seconds

    @property
    def hours(self):
        return self.__hours

    @property
    def minutes(self):
        return self.__minutes

    @property
    def seconds(self):
        return self.__seconds

    def __str__(self):
        """
        This method overwrites default implementation to return `Duration` object as str
        return: Str with the format 1m 10s, 1h 19m, 10h 1m 30s
        """
        time = []
        if self.hours != 0:
            time.append(str(self.hours) + 'h')
        if self.minutes != 0:
            time.append(str(self.minutes) + 'm')
        if self.seconds != 0:
            time.append(str(self.seconds) + 's')
        return ' '.join(time)

    def __lt__(self, other):
        """
        This method verify if the actual `Duration` object is less tham other Duration object.
        return: Bool.
        """
        total_seconds_d1 = get_total_seconds(self.hours, self.minutes, self.seconds)
        total_seconds_d2 = get_total_seconds(other.hours, other.minutes, other.seconds)
        return total_seconds_d1 < total_seconds_d2

    def __eq__(self, other):
        """
        This method verify if the actual `Duration` object is equal to other Duration object.
        return: Bool.
        """
        total_seconds_d1 = get_total_seconds(self.hours, self.minutes, self.seconds)
        total_seconds_d2 = get_total_seconds(other.hours, other.minutes, other.seconds)
        return total_seconds_d1 == total_seconds_d2


class Participant:
    """
    The class Participant repressents an attendee in a meeting ocurrence
    """

    def __init__(self, name, email, first_join, last_leave, role):
        """
        Create a new Participant object
        :param name: The name of the participant, str object
        :param email: The email of the participant, str object
        :param first_join: DateTimeWrapper object
        :param last_leave: DateTimeWrapper object
        param role: str object
        """
        self.__name = name
        self.__email = email
        self.__first_join = first_join
        self.__last_leave = last_leave
        self.__role = role

    @property
    def in_meeting_duration(self):
        """
        This is the property that gets the in-meeting duration
        :return: `Duration` object, the elapsed time between :attr:`__first_join` and
                                                             :attr:`__last_leave`
        """
        duration_tuple = calculate_duration_hh_mm_ss(self.__first_join, self.__last_leave)
        return Duration(*duration_tuple)

    @property
    def name(self):
        """
        This property returns the name of this participant
        :return: str of The value of :attr:`__name`
        """
        return self.__name

    @property
    def email(self):
        """
        This property returns the email of this participant
        :return: str of The value of :attr:`__email`
        """
        return self.__email

    @property
    def first_join(self):
        """
        This property returns the first_join of this participant
        :return: DateTimeWrapper Object of The value of :attr:`__first_join`
        """
        return self.__first_join

    @property
    def last_leave(self):
        """
        This property returns the last_leave of this participant
        :return: DateTimeWrapper Object of The value of :attr:`__last_leave`
        """
        return self.__last_leave

    @property
    def role(self):
        """
        This property returns the role of this participant
        :return: str of The value of :attr:`__role`
        """
        return self.__role


class MeetingOcurrence:
    """
    The MeetingOcurrence class represents a Team's meeting ocurrence
    """

    def __init__(self, attended, start_time, end_time):
        """
        Create a new MeetingOcurrence.
        :param attended: The number of participants
        :param start_time: The meeting start time, DateTimeWrapper object
        :param end_time: The meeting end time, DateTimeWrapper object
        """
        self.__attended = attended
        self.__start_time = start_time
        self.__end_time = end_time
        self.__participants = []

    @property
    def meeting_duration(self):
        """
        This is the property that gets the meeting duration
        :return: The meeting duration, elapsed time between :attr:`__start_time` and
        :attr:`__end_time` as a `Duration` object
        """
        duration_tuple = calculate_duration_hh_mm_ss(self.__start_time, self.__end_time)
        return Duration(*duration_tuple)

    @property
    def participants(self):
        """
        This property returns the list of participants
        :return: The value of :attr:`__participants`
        """
        return self.__participants

    def add_participants(self, participants: list[Participant]):
        """
        This method adds 1 or more participants in the :attr:`__participants`
        if `Participant.email` exists already update all Participant attributes except
        `Participant.email`
        :param participants: A list containing 1 or more `Participant` objects
        """
        for participant in participants:
            index = self.get_participant_index(participant.email)
            if index >= 0:
                self.__participants[index] = participant
            else:
                self.__participants.append(participant)

    def get_participant_index(self, email):
        """
        This method return the index of the participant with the same email in atrr:__participants
        :param: email, a str that is the email to be found in the list of participants.
        return: An Int that represents the position in __participants list
        """
        for index, participant in enumerate(self.__participants):
            if participant.email == email:
                return index
        return -1


class Meeting:
    """
    The Meeting represents a Team's meeting
    """

    def __init__(self, title):
        """
        Create a new Meetig object.
        :param title as a str: The title of the meeting
        """
        self.__title = title
        self.__attendance = []

    @property
    def title(self):
        return self.__title

    @property
    def attendance(self):
        """
        This property gets the list of meeting ocurrences
        :return: The value of :attr:`__attendance`
        """
        return self.__attendance

    def add_attendance(self, meeting_ocurrences):
        """
        This method adds 1 or more meeting ocurrences in the :attr:`__attendance`
        if any `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time` exists already in the
        :attr:`__attendance` then update all MeetingOcurrence attributes except
            `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time`
        :param meeting_ocurrences: A list containing 1 or more `MeetingOcurrence` objects
        """
        for meeting_ocurrence in meeting_ocurrences:
            index = self.get_meeting_ocurrence_index(meeting_ocurrence)
            if index >= 0:
                self.__attendance[index].attended = meeting_ocurrence.attended
                self.__attendance[index].participants = meeting_ocurrence.participants
            else:
                self.__participants.append(meeting_ocurrence)

    def get_meeting_ocurrence_index(self, meeting_ocurrence):
        """
        This method return the index of the meeting_ocurrence with the same
            start_time and end_time in atrr:__attendance.
        :param: meeting_ocurrence, a MeetingOcurrence Object.
        return: int, it represents the position of the meeting_ocurrence in attr:__attendance
        """
        start_time = meeting_ocurrence.start_time
        end_time = meeting_ocurrence.end_time
        for index, m_o in enumerate(self.__attendance):
            if m_o.start_time == start_time and m_o.end_time == end_time:
                return index
        return -1
