from src.teams_meeting import Duration


def test_output_format_duration_1():
    """ should return 5s when hour is 0, minute is 0 """
    d = Duration(0, 0, 5)
    assert str(d) == "5s"


def test_output_format_duration_2():
    """ should return 1h 26m 55s when hour is 1, minute is 26, second is 55 """
    d = Duration(1, 26, 55)
    str(d) == "1h 26m 55s"


def test_output_format_duration_3():
    """ should return 1m 26s when hour is 0, minute is 1, second is 26 """
    d = Duration(0, 1, 26)
    str(d) == "1m 26s"


def test_output_format_duration_4():
    """ should return 1h 42m when hour is 1, minute is 42, second is 0 """
    d = Duration(1, 42, 0)
    str(d) == "1h 42m"


def test_less_than_durations():
    """ should be True, 8m 47s is less than 28m 40s """
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d2 < d1, "expected d2 less than d1"


def test_greather_than_durations():
    """ should return True, 28m 40s is greater than 8m 47s """
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d1 > d2, "expected d1 greater than d2"


def test_equals_durations():
    """ should return True, 28m 40s is equals to 28m 40s """
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 28, 40)
    assert d2 == d1, "expected d2 equal to d1"


def test_not_equals_durations():
    """ should return True, 28m 40s is not equals to 8m 47s """
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d2 != d1


def test_order_list_of_durations():
    """ should order list of Duration element in ascendent order"""
    duration_list = [Duration(0, 8, 47), Duration(0, 28, 40), Duration(0, 1, 0)]
    sorted_duration_list = sorted(duration_list)
    assert sorted_duration_list[0].hours == 0 and sorted_duration_list[0].minutes == 1 \
        and sorted_duration_list[0].seconds == 0
    assert sorted_duration_list[-1].hours == 0 and sorted_duration_list[-1].minutes == 28 \
        and sorted_duration_list[-1].seconds == 40


def test_order_list_of_durations_2():
    """ should order list of Duration element in descendent order"""
    duration_list = [Duration(0, 8, 47), Duration(0, 28, 40), Duration(0, 1, 0)]
    sorted_duration_list = sorted(duration_list, reverse=True)
    assert sorted_duration_list[0].hours == 0 and sorted_duration_list[0].minutes == 28 \
        and sorted_duration_list[0].seconds == 40
    assert sorted_duration_list[-1].hours == 0 and sorted_duration_list[-1].minutes == 1 \
        and sorted_duration_list[-1].seconds == 0
