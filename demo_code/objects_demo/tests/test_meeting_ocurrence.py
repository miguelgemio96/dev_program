from src.teams_meeting import MeetingOcurrence, Participant
from src.helpers import DateTimeWrapper, FORMAT_LONG_YEAR


def test_create_new_instance():
    first_join = DateTimeWrapper("6/17/22, 10:09:57 AM")
    last_leave = DateTimeWrapper("6/17/22, 12:10:15 PM")
    maria = Participant("Maria Marquina Guevara", "Maria.Marquina@fundacion-jala.org",
                        first_join, last_leave, "Organizer")

    first_join = DateTimeWrapper("10/18/2022, 10:50:03 PM", FORMAT_LONG_YEAR)
    last_leave = DateTimeWrapper("10/18/2022, 11:53:52 PM", FORMAT_LONG_YEAR)
    salome = Participant("Salome Quispe Guarachi", "Salome.Quispe@fundacion-jala.org",
                         first_join, last_leave, "Presenter")

    participants = [maria, salome]

    start_time = DateTimeWrapper("6/17/22, 10:00:00 AM")
    end_time = DateTimeWrapper("6/17/22, 1:00:00 PM")
    meeting_ocurrence = MeetingOcurrence(2, start_time, end_time)

    meeting_ocurrence.add_participants(participants)

    assert meeting_ocurrence is not None, 'The result should be not None'
    assert len(meeting_ocurrence.participants) == 2
    assert meeting_ocurrence.meeting_duration is not None
    assert str(meeting_ocurrence.meeting_duration) == '3h'


def test_create_new_instance_2():
    first_join = DateTimeWrapper("6/17/22, 10:09:57 AM")
    last_leave = DateTimeWrapper("6/17/22, 12:10:15 PM")
    maria = Participant("Maria Marquina Guevara", "Maria.Marquina@fundacion-jala.org",
                        first_join, last_leave, "Organizer")

    first_join = DateTimeWrapper("10/18/2022, 10:50:03 PM", FORMAT_LONG_YEAR)
    last_leave = DateTimeWrapper("10/18/2022, 11:53:52 PM", FORMAT_LONG_YEAR)
    salome = Participant("Salome Quispe Guarachi", "Salome.Quispe@fundacion-jala.org",
                         first_join, last_leave, "Presenter")

    first_join = DateTimeWrapper("10/18/2022, 10:40:03 PM", FORMAT_LONG_YEAR)
    last_leave = DateTimeWrapper("10/18/2022, 11:33:52 PM", FORMAT_LONG_YEAR)
    salome_repeated = Participant("Salome Quispe Guarachi", "Salome.Quispe@fundacion-jala.org",
                                  first_join, last_leave, "Presenter")

    participants = [maria, salome]

    start_time = DateTimeWrapper("6/17/22, 10:00:00 AM")
    end_time = DateTimeWrapper("6/17/22, 1:00:00 PM")
    meeting_ocurrence = MeetingOcurrence(2, start_time, end_time)

    meeting_ocurrence.add_participants(participants)
    meeting_ocurrence.add_participants([salome_repeated])

    assert meeting_ocurrence is not None, 'The result should be not None'
    assert len(meeting_ocurrence.participants) == 2
    assert meeting_ocurrence.meeting_duration is not None
    assert str(meeting_ocurrence.meeting_duration) == '3h'
