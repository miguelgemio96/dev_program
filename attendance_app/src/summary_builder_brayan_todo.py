from datetime import datetime

FORMAT = '%m/%d/%y, %I:%M:%S %p'


def get_duration(start_time, end_time):
    start = datetime.strptime(start_time, FORMAT)
    end = datetime.strptime(end_time, FORMAT)
    # TODO capture end-start result in variable and use it when required
    seconds = int((end-start).total_seconds())
    minutes = int(((end-start).total_seconds())/60)
    hours = int(((end-start).total_seconds())/3600)

    # TODO You can write better, over-engineered and prone to error
    while minutes >= 60:
        minutes = minutes - 60
    while seconds >= 60:
        seconds = seconds - 60
    return {
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    }



def normalize_summary(raw_data: dict):

    # TODO: Refactor, Line 31-50 are same as line 52-71 with different labels/keys
    if not raw_data.get('Meeting Id'):
        title = raw_data.get('Meeting title')
        id = raw_data.get('Meeting title')
        attended_participants = raw_data.get('Attended participants') or 0
        start_time = raw_data.get('Start time')
        end_time = raw_data.get('End time')
        # TODO: use start_time, end_time
        duration = get_duration(str(raw_data.get('Start time')), str(raw_data.get('End time')))
        response = {
            'Title': title,
            'Id': id,
            'Attended participants': int(attended_participants),
            'Start Time': start_time,
            'End Time': end_time,
            'Duration': {
                'hours': duration.get('hours'),
                'minutes': duration.get('minutes'),
                'seconds': duration.get('seconds'),
            }
        }
        return response
      
    title = raw_data.get('Meeting Title')
    id = raw_data.get('Meeting Id')
    attended_participants = raw_data.get('Total Number of Participants')
    start_time = raw_data.get('Meeting Start Time')
    end_time = raw_data.get('Meeting End Time')
    # TODO: use start_time, end_time
    duration = get_duration(str(raw_data.get('Meeting Start Time')), str(raw_data.get('Meeting End Time')))
    response = {
        'Title': title,
        'Id': id,
        'Attended participants': int(attended_participants),
        'Start Time': start_time,
        'End Time': end_time,
        'Duration': {
            'hours': duration.get('hours'),
            'minutes': duration.get('minutes'),
            'seconds': duration.get('seconds'),
        }
    }
    return response















def build_summary_object(raw_data: dict):
    pass